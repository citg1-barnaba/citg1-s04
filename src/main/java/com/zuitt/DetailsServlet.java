package com.zuitt;


import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		//Branding - Servlet Context Parameter
		ServletContext srvContext = getServletContext();
		String branding  = srvContext.getInitParameter("branding");
		
		//Firstname - System Properties
		String firstname = System.getProperty("firstname");
		
		//Lastname - Http Session
		HttpSession session = req.getSession();
		String lastname = session.getAttribute("lastname").toString();
		
		//Email - Servlet Context
		String email = srvContext.getAttribute("email").toString();
		
		//Contact Number - URL Rewriting via sendRedirect
		String contact = req.getParameter("contact");
		
		PrintWriter out = res.getWriter();
		out.println(
			"<h1>" + branding + "</h1>" +
			"<p>First name: " + firstname + "</p>" +
			"<p>Last name: " + lastname + "</p>" +
			"<p>Email: " + email + "</p>" +
			"<p>Contact: " + contact + "</p>"
		);
		
		
	}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been destroy. ");
		System.out.println("******************************************");
	}

}
